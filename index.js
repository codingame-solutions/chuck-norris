//https://www.codingame.com/ide/puzzle/chuck-norris

const MESSAGE = readline();

let binaryMessage = MESSAGE.split('').map((messageChar) => {
  messageChar = messageChar.charCodeAt().toString(2);
  return ( messageChar.length < 7 ? '0'.repeat(7 - messageChar.length) : '' ) + messageChar;
}).join('');

binaryMessage = binaryMessage.match(/([0]+)|([1]+)/g).map((binariesGrouped) => {
  let firstChar = binariesGrouped.substring(0, 1);
  return(`${(firstChar === "0" ? "00" : "0")} ${binariesGrouped.replace(/(0)|(1)/g, "0")}`);
});
console.log(binaryMessage.join(' '));
